"use strict";
const config = require('config');
var mongoose = require('mongoose').set('debug', true);

// TO CONFIG
class MongoDal {
    constructor() {
        const mongoDbConnectionString = config.mongo.host + '/'+ config.mongo.db;
        mongoose.connect(mongoDbConnectionString, { useNewUrlParser: true, useUnifiedTopology: true});
        this._db = mongoose.connection;
        this._db.on('error', console.error.bind(console, 'MongoDB connection error:'));
        var Schema = mongoose.Schema;

        // CROPS
        var cropScheme = new Schema({
            _id: Schema.Types.ObjectId,
            cropId: String,
            footprintInWorld: String,
            footprintInImage: String,
            imageId: String,
            classes: [String],
            cropImage: Buffer,
            version: Number
        },{versionKey: false});
        this.CropModel = mongoose.model('Crop', cropScheme );

        var objectScheme = new Schema({
            _id: Schema.Types.ObjectId,
            objectId: String,
            cropId: String,
            objectWorldBound: String,
            cropImageBound: String,
            middlePoint: String,
            classId: String,
            version: Number
        },{versionKey: false});
        this.ObjectModel = mongoose.model('Object', objectScheme);

        var classScheme = new Schema({
          _id: Schema.Types.ObjectId,
          classId: String,
          hebrewName: String
        },{versionKey: false});
        this.ClassModel = mongoose.model('Class', classScheme);
    }

  //Crops
    getCropsByClassId(classId) {
        let crops = this.CropModel.find().where('classes').all([classId]).select('-cropImage');
        return crops;
    }

    getCropMetadataById(cropId) { 
        let crop = this.CropModel.findOne({ cropId });
        return crop;
    }
    
    createCrop(crop) {
      crop.cropId = new mongoose.mongo.ObjectId();
      crop._id = crop.cropId;
      let cropDocument = new this.CropModel(crop);
      return [cropDocument.save(), crop.cropId];
    }
    
    updateCropById(cropId, updatedCrop){
      return this.CropModel.findOneAndUpdate({cropId}, updatedCrop, {useFindAndModify: false});
    }
    //////////////////////

  // Objects
    countClassObjects(classId){
      return this.ObjectModel.countDocuments({classId});
    }

    deleteObjectById(objectId) {
        return this.ObjectModel.findOneAndDelete({objectId});
    }

    
    createObject(object){
      object.objectId = new mongoose.mongo.ObjectId();
      object._id = object.objectId;
      let objectDocument = new this.ObjectModel(object);
      return objectDocument.save();
    }

    getObjectById(objectId){
      let object = this.ObjectModel.findOne({objectId});
      return object;
    }

    updateObjectById(objectId, updatedObject){
      return this.ObjectModel.findOneAndUpdate({objectId}, updatedObject, {useFindAndModify: false});
    }

    getObjectsByCropId(cropId){
      let objects = this.ObjectModel.find({cropId});
      return objects;
    }
    ////////////////////////////////////////////
    
    
    // Classes
    createClass(clas) {
      clas.classId = new mongoose.mongo.ObjectId();
      clas._id = clas.classId;
      let classDocument = new this.ClassModel(clas);
      return classDocument.save();
    }
    
    getAllClasses() {
      let classes = this.ClassModel.find();
      return classes;
    }
    //////////////////////////////////////////
  }
    
// Used so only one connection to mongo would be made
let singleton;

let getInstance = function() {
  
  if (!singleton) {
    singleton = new MongoDal();
  }

  return singleton;
}


module.exports = getInstance;