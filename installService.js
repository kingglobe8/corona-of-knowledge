var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'TOK mock',
  description: 'Tree of knowledge mocking service for sampling',
  script: 'C:\\Pixel\\corona-of-knowledge\\index.js',
  env: {
      name: "NODE_ENV",
      value: "development"
  }
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});

svc.install();