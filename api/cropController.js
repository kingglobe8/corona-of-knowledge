"use strict";
const dal = require("../mongoDal.js")();

const path = require('path');
const fs = require('fs');

let getCropsByClassId = async function(req, res) {
    try {
        let classId = req.query.classId;
        let crops = [];
        crops = await dal.getCropsByClassId(classId);

        if (crops.length === 0) {
            return res.status(404).send(`Crops with classId: ${classId} were not found.`);
        }

        return res.json(crops);
    } catch(err) {
        return res.status(500).send(err.message);
    }
}

let getCropMetadataById = async function(req, res) {
    try {
        let cropId = req.query.cropId;
        let crop = await dal.getCropMetadataById(cropId);

        if (!crop) {
            return res.status(404).send(`Crop with cropId: ${cropId} was not found.`);
        }

        return res.json(crop);
    } catch(err) {
        return res.status(500).send(err.message);
    }
}

let getCropImageById = async function(req, res) {
    try {
        let cropId = req.query.cropId;

        let dirPath = path.resolve('crops', cropId);

        if (!fs.existsSync(dirPath)) {
            return res.status(404).send(`Crop with cropId: ${cropId} was not found.`);
        }

        let fileNames = await fs.promises.readdir(dirPath);
        let imageFileName = fileNames[0];
        
        let fileData = await fs.promises.readFile(path.resolve(dirPath, imageFileName));
        return res.status(200).contentType('image/*').send(fileData);
    } catch(err) {
        return res.status(500).send(err.message);
    }
}

let postCrop = async function(req, res) {
    try {
        let cropMetadata = typeof(req.body.cropMetadata) === 'string' ?
                           JSON.parse(req.body.cropMetadata) :
                           req.body.cropMetadata; // express package can't handle multipart well
        let cropImage = req.files.cropImage;
        let [saveMetadataPromise, cropId] = dal.createCrop(cropMetadata);

        let imageDirPath = path.resolve('crops', cropId.toString());
        await fs.promises.mkdir(imageDirPath, {recursive: true});

        let filePath = path.resolve(imageDirPath, cropImage.name);
        fs.writeFileSync(filePath, cropImage.data);

        await saveMetadataPromise;
        return res.status(200).json(cropId);
    } catch(err) {
        return res.status(500).send(err.message);
    }
}

let updateCrop = async function(req, res) {
    try {
        let updatedCrop = req.body;
        let cropId = req.body.cropId;
        await dal.updateCropById(cropId, updatedCrop);
        return res.status(200).json(cropId);
    } catch(err) {
        return res.status(500).send(err.message);
    }
}




module.exports = { getCropsByClassId, getCropMetadataById, getCropImageById, postCrop, updateCrop};
