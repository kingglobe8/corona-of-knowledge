openapi: 3.0.2
info:
  title: Cloud of Knowledge Sampling Simulator
  description: Online mock for cloud of knowledge
  version: 0.1.1

paths:
  /crops/getCropsByClassId:
    get:
      summary: Returns all crops by profile unqiue ID
      operationId: getCropsByClassId
      parameters:
        - name: classId
          in: query
          required: true
          description: class ID
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemes/Crop'
        '404':
          description: A class with the specified ID was not found.
        default:
          description: Unexpected error

  /crops/getCropMetadataById:
    get:
      summary: get crop metadata by ID
      operationId: getCropMetadataById
      parameters:
        - name: cropId
          in: query
          required: true
          description: crop ID
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemes/Crop'
        '404':
          description: A crop with the specified ID was not found.
        default:
          description: Unexpected error

  /crops/getCropImageById:
    get:
      summary: get crop image by ID
      operationId: getCropImageById
      parameters:
        - name: cropId
          in: query
          required: true
          description: crop ID
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            image/*:
              schema:
                type: string
                format: binary
        '404':
          description: A crop with the specified ID was not found.
        default:
          description: Unexpected error 

  /crops/createCrop:
    post:
      summary: create new crop
      operationId: postCrop
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                cropMetadata:
                  $ref: '#/components/schemes/Crop'
                cropImage:
                  type: string
                  format: binary
              
      responses:
        '200':
          description: Success
          content:
            application/json:
              description: Crop ID
              schema:
                type: string
        default:
          description: Unexpected error

  /crops/updateCrop:
    post:
      summary: update crop
      operationId: updateCrop
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemes/Crop'
      responses:
        '200':
          description: updated
        default:
          description: Unexpected error
  
  /classes/getClasses:
    get:
      summary: Returns all classes
      operationId: getClasses
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemes/Class'
        default:
          description: Unexpected error

  /classes/postClasses:
    post:
      summary: Create new classes
      operationId: postClasses
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemes/Class'
      responses:
        '201':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemes/Class'
        default:
          description: Unexpected error

  /objects/countClassObjects:
    get:
      summary: count objects in given class
      operationId: countClassObjects
      parameters:
        - name: classId
          in: query
          required: true
          description: class Id
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: integer
        default:
          description: Unexpected error

  /objects/createObject:
    post:
      summary: create new object
      operationId: postObject
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemes/Object'
      responses:
        '201':
          description: Created
        default:
          description: Unexpected error

  /objects/deleteObject:
    delete:
      summary: delete object
      operationId: deleteObject
      parameters:
        - name: objectId
          in: query
          required: true
          description: object Id
          schema:
            type: string
      responses:
        '201':
          description: Deleted
        default:
          description: Unexpected error

  /objects/updateObject:
    post:
      summary: update object
      operationId: updateObject
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemes/Object'
      responses:
        '201':
          description: updated
        default:
          description: Unexpected error

  /objects/getObjectById:
    get:
      summary: get object by ID
      operationId: getObjectById
      parameters:
        - name: objectId
          in: query
          required: true
          description: object ID
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemes/Object'
        '404':
          description: An object with the specified ID was not found.
        default:
          description: Unexpected error

  /objects/getObjectsByCropId:
    get:
      summary: get object by  crop
      operationId: getObjectsByCropId
      parameters:
        - name: cropId
          in: query
          required: true
          description: crop ID
          schema:
            type: string
      responses:
        '200':
          description: OK
          application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemes/Object'
        '404':
          description: An object with the specified crop was not found.
        default:
          description: Unexpected error

components:
  schemes:
    Crop:
      type: object
      properties:
        cropId:
          type: string
        footprintInWorld:
          type: string
        footprintInImage:
          type: string
        classes:
          type: array
          items:
            type: string
        imageId:
          type: string
        version:
          type: integer
    Class:
      type: object
      properties:
        classId:
          type: string
        hebrewName:
          type: string
    Object:
      type: object
      properties:
        objectId:
          type: string
        cropId:
          type: string
        objectWorldBound:
          type: string
        cropImageBound:
          type: string
        middlePoint:
          type: string
        classId:
          type: string
        version:
          type: integer