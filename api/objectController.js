"use strict";
const dal = require("../mongoDal.js")();

let countClassObjects = async function(req, res) {
    try {
        let classId = req.query.classId;
        let count = await dal.countClassObjects(classId);
        return res.status(201).json(count);
    } catch(err) {
        return res.status(500).send(err.message);
    }
  }
  
  let postObject = async function(req, res) {
      try {
          let object = req.body;
          await dal.createObject(object);
          return res.status(201).json(object);
      } catch(err) {
          return res.status(500).send(err.message);
      }
  }
  
  let deleteObject = async function(req, res) {
      try {
          let objectId = req.query.objectId;
          await dal.deleteObjectById(objectId);
          return res.status(201).json(objectId);
      } catch(err) {
          return res.status(500).send(err.message);
      }
  }
  
  let updateObject = async function(req, res) {
      try {
          let updatedObject = req.body;
          let objectId = req.body.objectId;
          await dal.updateObjectById(objectId, updatedObject);
          return res.status(201).json(objectId);
      } catch(err) {
          return res.status(500).send(err.message);
      }
  }
  
  let getObjectById = async function(req, res) {
    try {
        let objectId = req.query.objectId;
        let object = await dal.getObjectById(objectId);
  
        if (object == null ) {
            return res.status(404).send(`Object with id: ${objectId} was not found.`);
        }
        return res.json(object);
    } catch(err) {
        return res.status(500).send(err.message);
    }
  }

  let getObjectsByCropId = async function(req, res) {
    try {
        let cropId = req.query.cropId;
        let objects = await dal.getObjectsByCropId(cropId);
  
        if (objects.length === 0) {
          return res.status(404).send(`Objects with cropId: ${cropId} were not found.`);
        }
  
        return res.json(objects);
    } catch(err) {
        return res.status(500).send(err.message);
    }
  }

  module.exports = { countClassObjects, postObject, deleteObject, updateObject, getObjectById, getObjectsByCropId};