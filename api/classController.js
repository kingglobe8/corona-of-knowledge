"use strict";
const dal = require("../mongoDal.js")();

let getClasses = async function(req, res) {
    try {
        let classes = await dal.getAllClasses();

        return res.json(classes);
    } catch(err) {
        return res.status(500).send(err.message);
    }
}

let postClasses = async function(req, res) {
    try {
        req.body.forEach(async function(clas){
            await dal.createClass(clas)
        });
        return res.status(200).json(req.body);
    } catch(err) {
        return res.status(500).send(err.message);
    }
}


module.exports = { getClasses, postClasses};
