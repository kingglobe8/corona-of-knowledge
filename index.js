"use strict";
const config = require('config');
const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./api/swagger.yaml');

const { connector } = require('swagger-routes-express');
const cropApi = require('./api/cropController');
const classApi = require('./api/classController');
const objectApi = require('./api/objectController');

let unifiedAPi = {...cropApi, ...classApi, ...objectApi};

// TO CONFIG
const port = process.env.PORT || 80;

app.use(fileUpload());
app.use(express.json({ limit: '20mb'}));
app.use(express.urlencoded({extended: false, limit: '20mb'}));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
const connect = connector(unifiedAPi, swaggerDocument);
connect(app);
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));